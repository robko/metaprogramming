#ifndef MAKE_VARIADIC_HPP
#define MAKE_VARIADIC_HPP

#include <functional>
#include <type_traits>

// idea: https://www.youtube.com/watch?v=Mzi5SHi13MM

// two argument version
template <
		typename Tfun, // call object type
		typename Targ // argument type
		>
typename std::result_of<Tfun(Targ, Targ)>::type // return type
	make_variadic(Tfun fun, Targ &&arg1, Targ &&arg2) {
			return fun(std::forward<Targ>(arg1), std::forward<Targ>(arg2));
}

// variadic version
template <
		typename Tfun, // call object type
		typename Targ, // argument type
		typename... Targs // arguments types
		>
// return type same as above
typename std::result_of<Tfun(Targ, Targ)>::type // return type
	make_variadic(Tfun fun, Targ &&arg1, Targs&& ...args) {
	// call recursuve this function or two argument version
	return fun(arg1, make_variadic(fun, std::forward<Targs>(args)...));
}

#endif // MAKE_VARIADIC_HPP

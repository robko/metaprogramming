#include <algorithm>
#include <cmath>
#include <iostream>
#include "make_variadic.hpp"

using namespace std;

void test1() {
	auto max_fun_lambda = [](auto a, auto b) {
		return std::max(a, b);
	};
	std::cout << make_variadic(max_fun_lambda, 29, 20, 3) << '\n';
	std::cout << make_variadic(max_fun_lambda, 234.877, 2.543, -8.4565) << '\n';

	std::function<const int& (const int&, const int&)> max_fun =
			static_cast<const int& (*)(const int&, const int&)>(&std::max<int>);
	std::cout << make_variadic(max_fun, 29, 202, 65) << '\n';


	auto fmin_fun_lambda = [](auto a, auto b) {
		return std::fmin(a, b);
	};

	std::cout << make_variadic(fmin_fun_lambda, 29, -3.777, 9876542.5) << '\n';
}

int main() {
	test1();
	return 0;
}

